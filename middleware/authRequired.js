module.exports = {
  authRequired(req, res, next) {
    if (req.staff) {
      return next()
    }
    throw { message: 'Unauthorized access', status: 401 }
  },
}
