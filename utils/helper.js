const bcrypt = require('bcryptjs')
const dayjs = require('dayjs')
const utc = require('dayjs/plugin/utc')
const timezone = require('dayjs/plugin/timezone')

dayjs.extend(utc)
dayjs.extend(timezone)

module.exports = {
  validateEmail: function (email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    return re.test(email)
  },

  onCron(cb, seconds) {
    setInterval(() => {
      cb()
    }, [seconds * 1000])
  },

  getCurrentTime() {
    return dayjs().tz('Asia/Hong_Kong').format('DD/MM/YYYY HH:mm:ss')
  },

  hashPassword(password) {
    const saltRounds = 10
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(saltRounds, (saltError, salt) => {
        if (saltError) {
          throw saltError
        } else {
          bcrypt.hash(password, saltRounds, (err, hash) => {
            if (err) {
              throw err
            }
            resolve(hash)
          })
        }
      })
    })
  },
}
