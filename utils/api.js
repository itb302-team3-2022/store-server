
module.exports = {
  ApiKey: process.env.API_LOGISTIC_KEY,
  ApiBase: process.env.API_LOGISTIC_BASE,
};
