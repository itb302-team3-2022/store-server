const express = require('express')
const createError = require('http-errors')
const dotenv = require('dotenv').config()
const cors = require('cors')
const cookieParser = require('cookie-parser')
const jsonwebtoken = require('jsonwebtoken')
const { getCurrentTime, onCron } = require('./utils/helper')
const Order = require('./Models/Order.model')
const api = require('./utils/api')

const app = express()
app.use(cookieParser())

app.use(
  cors({
    credentials: true,
    origin: 'http://team3store.com:3500',
  })
)

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Credentials', true)
  res.header('Access-Control-Allow-Origin', req.headers.origin)
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  res.header(
    'Access-Control-Allow-Headers',
    'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept'
  )
  next()
})

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Initialize DB
require('./initDB')()

app.use((req, _, next) => {
  if (req.cookies.auth) {
    const jwt = req.cookies.auth.split(' ')[1]

    jsonwebtoken.verify(jwt, process.env.JWT_SECRET, (err, decode) => {
      if (err) req.staff = undefined
      req.staff = decode
      next()
    })
  } else {
    req.staff = undefined
    next()
  }
})

const ProductRoute = require('./Routes/Product.route')
const CustomerRoute = require('./Routes/Customer.route')
const OrderRoute = require('./Routes/Order.route')
const ShipRoute = require('./Routes/Ship.route')
const StaffRoute = require('./Routes/Staff.route')
const { default: axios } = require('axios')
app.use('/api/products', ProductRoute)
app.use('/api/customers', CustomerRoute)
app.use('/api/staff', StaffRoute)
app.use('/api/orders', OrderRoute)
app.use('/api/ship', ShipRoute)

const fetchShippment = async () => {
  const results = await Order.where('status')
    .ne('shipped')
    .find({
      shipping: { $exists: true },
    })

  try {
    if (results.length > 0) {
      await Promise.all(
        results.map((r) => {
          return new Promise(async (resolve, reject) => {
            try {
              const resp = await axios.get(
                `${api.ApiBase}/shipment?tid=${r.shipping.trackId}`
              )
              const { data } = resp
              const record = data[0]
              if (!resp || !record) {
                throw Error({ message: 'fetched error' })
              }
              const order = await Order.findById(r._id)
              order.shipping.shipmentStatusType = record.shipmentStatusType
              if (record.shipmentStatusType === 'shipped') {
                order.status = 'completed'
              }
              if (record.shipmentStatusType === 'rejected') {
                order.status = 'rejected'
              }

              order.shipping.charge = record.charge
              order.shipping.shipmentTimeline = record.shipmentTimeline
              console.log(
                `[${getCurrentTime()}] `,
                'TrackID: ',
                order.shipping.trackId,
                'updated'
              )
              await order.save()
              resolve()
            } catch (err) {
              reject(err)
            }
          })
        })
      )
    }
  } catch {}
}

onCron(fetchShippment, 5)

//404 handler and pass to error handler
app.use((req, res, next) => {
  /*
  const err = new Error('Not found');
  err.status = 404;
  next(err);
  */
  next(createError(404, 'Not found'))
})

//Error handler
app.use((err, req, res, next) => {
  res.status(err.status || 500)
  res.send({
    error: {
      status: err.status || 500,
      message: err.message,
    },
  })
})

const PORT = process.env.PORT || 3000

app.listen(PORT, () => {
  console.log('Server started on port ' + PORT + '...')
})
