const express = require('express')
const router = express.Router()

const ShipController = require('../Controllers/Ship.Controller')

//Create a new shipment
router.post('/', ShipController.createNewShipment)

module.exports = router
