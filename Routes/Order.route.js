const express = require('express')
const router = express.Router()
const { authRequired } = require('../middleware/authRequired')

const OrderController = require('../Controllers/Order.Controller')

//Get a list of all orders
router.get('/', authRequired, OrderController.getAllOrders)

//Create a new order
router.post('/', authRequired, OrderController.createNewOrder)

module.exports = router
