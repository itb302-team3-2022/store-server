const express = require('express')
const router = express.Router()
const { authRequired } = require('../middleware/authRequired')

const CustomerController = require('../Controllers/Customer.Controller')

//Get a list of all customers
router.get('/', authRequired, CustomerController.getAllCustomers)

//Create a new customer
router.post('/', CustomerController.createNewCustomer)

module.exports = router
