const express = require('express')
const router = express.Router()

const StaffController = require('../Controllers/Staff.Controller')

router.post('/', StaffController.createStaff)
router.post('/login', StaffController.login)

module.exports = router
