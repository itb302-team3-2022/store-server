const createError = require('http-errors')

const Customer = require('../Models/Customer.model')

module.exports = {
  getAllCustomers: async (req, res, next) => {
    let customerIds = []
    if (req.query.id) {
      customerIds = req.query.id.split(',')
    }

    try {
      let results = []
      if (customerIds.length > 0) {
        results = await Promise.all(
          customerIds.map(async (id) => {
            return await Customer.findById(id)
          })
        )
      } else {
        results = await Customer.find()
      }

      res.send({ data: results, auth: req.staff })
    } catch (error) {
      console.log(error.message)
      next(error)
    }
  },

  createNewCustomer: async (req, res, next) => {
    try {
      const product = new Customer(req.body)
      const result = await product.save()
      res.send(result)
    } catch (error) {
      console.log(error.message)
      if (error.name === 'ValidationError') {
        next(createError(422, error.message))
        return
      }
      next(error)
    }
  },
}
