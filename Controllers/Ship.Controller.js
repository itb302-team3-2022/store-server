const api = require('../utils/api')
const axios = require('axios')
const Product = require('../Models/Product.model')
const Customer = require('../Models/Customer.model')
const Order = require('../Models/Order.model')

module.exports = {
  createNewShipment: async (req, res, next) => {
    const { body } = req

    /**
     * Flow
     * 1. Pull shipping info if isn't given
     * 2. Send delivery request to logistic company
     * 3. If status (201) return
     *    a. Update db
     *    b. throw error
     *
     */

    if (!body.pickUp || !body.orderId || !body.parcels) {
      throw Error('something missing')
    }

    // grab destination detail if shipping not given
    try {
      const order = await Order.findById(body.orderId)
      const logisticOrder = {
        apiKey: api.ApiKey,
        shipTo: body.shipTo || {
          name: order.delivery.receiver,
          phone: order.delivery.phone,
          phone2: order.delivery.phone2,
          address: order.delivery.address,
        },
        shipFrom: body.shipFrom,
        parcels: body.parcels,
        pickUp: body.pickUp,
      }

      const resp = await axios.post(`${api.ApiBase}/shipment`, logisticOrder)

      const shipment = resp.data

      order.shipping = {
        trackId: shipment.trackId,
        shipFrom: logisticOrder.shipFrom,
        shipTo: {
          ...logisticOrder.shipTo,
          receiver: order.delivery.receiver,
        },
        parcels: logisticOrder.parcels,
        pickUp: logisticOrder.pickUp,
        status: shipment.shipmentStatusType,
        shipmentTimeline: shipment.shipmentTimeline,
      }

      order.status = 'processing'

      await order.save()
      res.send({
        orderId: order._id,
        trackId: order.shipping.trackId,
        status: order.status,
        shipmentStatusType: order.shipping.shipmentStatusType,
        shipmentTimeline: order.shipping.shipmentTimeline,
        timestamp: Date.now(),
      })
    } catch (err) {
      console.log('err', err)
      next(err)
    }
  },
}
