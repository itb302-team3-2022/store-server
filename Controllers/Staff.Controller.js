const { hashPassword } = require('../utils/helper')
const jwt = require('jsonwebtoken')
const Staff = require('../Models/Staff.model')

module.exports = {
  createStaff: async (req, res, next) => {
    try {
      const data = req.body
      data.password = await hashPassword(data.password)
      const staff = new Staff(data)
      await staff.save()

      res.status(201).send({ created: staff })
    } catch (err) {
      next(err)
    }
  },
  login: async (req, res, next) => {
    try {
      const data = req.body

      const staff = await Staff.findOne({
        username: data.username,
      })

      if (!staff) {
        throw {
          message: 'Authentication failed. Invalid user or password.',
          status: 403,
        }
      }

      const jwtPayload = { _id: staff._id, firstname: staff.firstname }

      const token = jwt.sign(jwtPayload, process.env.JWT_SECRET, {
        expiresIn: '1h',
      })

      res.status(201).cookie('auth', 'Bearer ' + token, {
        expires: new Date(Date.now() + 8 * 3600000), // cookie will be removed after 8 hours
      })

      return res.json({ staff: jwtPayload })
    } catch (err) {
      next(err)
    }
  },
}
