const createError = require('http-errors')
const mongoose = require('mongoose')

const Product = require('../Models/Product.model')
const Customer = require('../Models/Customer.model')
const Order = require('../Models/Order.model')

module.exports = {
  getAllOrders: async (req, res, next) => {
    console.log('enter')
    let orderIds = []
    if (req.query.id) {
      orderIds = req.query.id.split(',')
    }

    try {
      let results = []
      if (orderIds.length > 0) {
        results = await Promise.all(
          orderIds.map(async (id) => {
            return await Order.findById(id)
              .populate('customer')
              .sort({ createdAt: -1 })
              .populate({
                path: 'items',
                populate: {
                  path: 'productId',
                  model: Product,
                },
              })
          })
        )
      } else {
        results = await Order.find()
          .sort({ createdAt: -1 })
          .populate('customer')
          .populate({
            path: 'items',
            populate: {
              path: 'productId',
              model: Product,
            },
          })
      }

      res.send({ data: results, auth: req.staff })
    } catch (error) {
      next(error)
    }
  },

  createNewOrder: async (req, res, next) => {
    let totalAmount = 0
    let customer = {}
    const newOrder = req.body

    // get customer info
    try {
      customer = await Customer.findById(newOrder.customer)
      if (!customer) {
        throw Error('customer not exist')
      }
    } catch (err) {
      next(err)
    }

    // 1. retrieve product price
    // 2. calculate total amount
    const _items = await Promise.all(
      newOrder.items.map(async (i) => {
        try {
          const result = await Product.findById(i.productId)
          totalAmount += +result.price * i.qty
          return { ...i, price: +result.price }
        } catch (err) {
          return null
        }
      })
    )
    newOrder.items = _items.filter((i) => i !== null)
    newOrder.totalAmount = +totalAmount

    if (!newOrder.delivery) {
      newOrder.delivery = customer.delivery
    }
    console.log(newOrder)

    // new new order
    try {
      const result = await Order.create(newOrder)
      customer.order.push(result._id)
      customer.save()

      res.send(result)
    } catch (err) {
      next(err)
    }
  },
}
