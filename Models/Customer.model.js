const mongoose = require('mongoose')
const helper = require('../utils/helper')

const deliverySchema = new mongoose.Schema({
  receiver: String,
  phone: String,
  phone2: String,
  address: String,
})

const customerSchema = new mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      trim: true,
      lowercase: true,
      unique: true,
      required: 'Email address is required',
      validate: [helper.validateEmail, 'Please fill a valid email address'],
      match: [
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        'Please fill a valid email address',
      ],
    },
    order: [
      {
        type: mongoose.SchemaTypes.ObjectId,
        required: true,
        ref: 'Order',
      },
    ],
    delivery: deliverySchema,
  },
  {
    timestamps: true,
  }
)

module.exports =
  mongoose.models.Customer || mongoose.model('Customer', customerSchema)

module.exports.deliverySchema = deliverySchema
