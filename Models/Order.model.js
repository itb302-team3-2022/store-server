const mongoose = require('mongoose')
const deliverySchema = require('./Customer.model').deliverySchema

const itemSchema = new mongoose.Schema({
  productId: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true,
    ref: 'Product',
  },
  qty: {
    type: Number,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
})

const shippingSchema = new mongoose.Schema({
  trackId: String,
  shipFrom: {
    name: String,
    phone: String,
    phone2: String,
    address: String,
  },
  shipTo: {
    receiver: String,
    phone: String,
    phone2: String,
    address: String,
  },
  parcels: [
    {
      refId: String,
      weight: {
        value: Number,
        unit: {
          type: String,
          enum: ['kg', 'lb', 'g'],
          default: 'kg',
        },
      },
      dimension: {
        depth: Number,
        width: Number,
        height: Number,
        unit: {
          type: String,
          enum: ['cm', 'ft', 'inch', 'm'],
          default: 'cm',
        },
      },
      description: String,
      insuredValue: {
        amount: Number,
        currency: String,
      },
    },
  ],
  pickUp: {
    startTime: Date,
    endTime: Date,
  },
  status: {
    type: String,
    enum: ['pending', 'processing', 'completed', 'rejected'],
  }, // accepted or rejected or waiting
  charge: Number,
  shipmentStatusType: {
    type: String,
    enum: ['pending', 'preparing', 'picking', 'shipping', 'shipped'],
  },
  shipmentTimeline: [
    {
      time: Date,
      title: String,
      message: String,
      eventStatus: {
        type: String,
        enum: ['pending', 'preparing', 'picking', 'shipping', 'shipped'],
      },
    },
  ], // ordered, picked, shipping, sented
})

const orderSchema = new mongoose.Schema(
  {
    totalAmount: {
      type: Number,
      required: true,
    },
    customer: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      ref: 'Customer',
    },
    items: [itemSchema],
    paymentMethods: String,
    paymentRef: String,
    delivery: deliverySchema,
    status: {
      type: String,
      required: true,
      default: () => 'pending', // pending, processing, completed
    },
    shipping: shippingSchema,
  },
  {
    timestamps: true,
  }
)

module.exports = mongoose.models.Order || mongoose.model('Order', orderSchema)
