const mongoose = require('mongoose')

const productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    images: [String],
    description: String,
    price: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
)

module.exports =
  mongoose.models.Product || mongoose.model('Product', productSchema)
